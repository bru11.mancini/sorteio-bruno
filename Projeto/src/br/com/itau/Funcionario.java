package br.com.itau;

public class Funcionario {

    public enum Cargo {
        //ENUM SEMPRE MAIUSCULO
        DESENVOLVEDOR,
        TESTER,
        GESTOR
    }


    // && e
    // || ou
    //!= diferente

    public static void main(String[] args) {
        String nome = "Jeff";
        double salario = 10000.00;
        double porcentagemBonus = 0;
        Cargo cargo = Cargo.TESTER;

        //      boolean ehGestor = false;
        //     !ehGestor  -> se ficar assim, mudei a variavel para true

        if (cargo.equals("GESTOR")){
            porcentagemBonus = 0.85;
        }

        if (salario >= 1000 && salario <= 5000){
            porcentagemBonus = 0.90;
        }

    }
}
