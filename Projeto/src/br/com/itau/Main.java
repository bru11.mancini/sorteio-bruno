package br.com.itau;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Olá Mundo!");

        //Bloco de quando eu quiser usaro ramdom.
        //Random random = new Random();
        //int numero = random.nextInt(200);


        //O scanner utiliza os dados imputados pelo usuario.
        Scanner entradaDoConsole = new Scanner(System.in);
        System.out.println("Digite o numero");
        int numero = entradaDoConsole.nextInt();

        System.out.println(numero);

        if (numero % 2 == 0){
            System.out.println("Esse número é par");
        } else {
            System.out.println("Esse número é ímpar");
        }
    }
}
