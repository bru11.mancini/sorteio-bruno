package br.com.itau;

import java.util.Random;

public class Sorteio {
    public static void main(String[] args) {
        System.out.println("Começouuu");
        System.out.println("Exercício 1");

        //Bloco de quando eu quiser usaro ramdom.
        Random random = new Random();
        int numero = random.nextInt(6) + 1;
        System.out.println("O número sorteado é o " + numero);


        //bloco do exercicio 2

        System.out.println("");
        System.out.println("Exercício 2");
        int soma = 0;
        Random random1 = new Random();

        for(int x = 1; x <= 3; x++) {

            int numero2 = random1.nextInt(6) + 1;
            System.out.println("Número " + x + " é o " + numero2);
            soma += numero2;
        }
        System.out.println("A soma dos números é : " + soma);

        System.out.println("");
        System.out.println("Exercício 3");

        Random random2 = new Random();

        for(int y = 1; y <= 3; y++) {
            int soma2 = 0;
            System.out.println("Grupo " + y);
            for (int z = 1; z <= 3; z++) {
                int numero3 = random2.nextInt(6) + 1;
                System.out.println("Número " + z + " é o " + numero3);
                soma2 += numero3;
            }
            System.out.println("A soma dos números é : " + soma2);
            System.out.println("");
        }


    }
}
