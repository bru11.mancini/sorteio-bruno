package br.com.itau;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Listas {

    public static void main(String[] args) {
        String[] nomes = {"Gabriel", "Vini", "Jef"};
        System.out.println(nomes[1]);
        System.out.println(nomes.length);

        int[] idades = new int[3];
        idades[0] = 16;
        idades[1] = 29;
        //Esse [2] é só pra mostrar que a ide ja avisa que vai dar erro porque o limite eram 2 indices.
        idades[2] = 40;

        for (int i = 0; i < nomes.length; i++) {
            System.out.println(nomes[i]);
            System.out.println(idades[i]);
        }

        //Esse abaixo é o foreach..começou no java8
        for(String nome : nomes) {
            System.out.println("Esse é o nome " + nome);
        }

        //Esse é o exemplo do while
        int contador = 1;
        while (contador <=5){
            System.out.println(contador);
            contador += 1;
        }

        //Exemplo do Do
        int contador2 = 1;
        do {
            System.out.println(contador2);
            contador2 += 1;
        }
            while (contador2 <= 10);

        //        if (nome.equals("Gabriel")){
//            System.out.println("Opa, e ai Gabriel");
//        }

        //Exemplo da Lista add
        List lista = new ArrayList();
        lista.add("Joao");
        lista.add("Bruno");
        System.out.println(lista.get(0));
        System.out.println(lista.get(1));


        //Exemplo do map
        Map<Integer, String> funcionario = new HashMap<>();
        funcionario.put(1, "Dev");
        funcionario.put(3, "Met");
        System.out.println(funcionario.get(0));
        System.out.println(funcionario.get(1));
        System.out.println(funcionario.get(2));
        System.out.println(funcionario.get(3));
        System.out.println(funcionario.get(300));




    }
}
